There are 4 files in this folder used to create an AD Infrastucture

setup_ad_step1.ps1 

Sets up the base computer environment with a computername, IP Information and Timezone
AD needs a static IP Address.

Will reboot and change the IP Address

setup_ad_step2.ps1

Installs the software needed to create an AD Domain, as the first DC in a Forest
Also sets up the admin users password.
When AD is installed DNS is also installed for the provided domain name.

Will reboot, keeps IP Address

setup_ad_step3.ps1

This script is optional, however recommended as it sets up a user with domain admin
privilages other scripts can be run as instead of the administrator account

variables.ps1

This is a text file which feeds the variables needed by the above scripts to provide
specific server setup.
