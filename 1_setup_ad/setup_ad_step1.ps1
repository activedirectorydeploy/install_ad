##Powershell Script to setup AD on Windows server 2012 or 2016
#this script will setup the base environment
# Import Variables
. ../Variables.ps1
Rename-computer -newname $computername
Get-NetAdapter
$ipaddress = $myip
$dnsaddress = $mydns
New-NetIPAddress -InterfaceAlias Ethernet -IPAddress $ipaddress -AddressFamily IPv4 -PrefixLength 24
Set-DnsClientServerAddress -InterfaceAlias Ethernet -ServerAddresses $dnsaddress
Set-TimeZone -Id $mytimezone
Restart-Computer
