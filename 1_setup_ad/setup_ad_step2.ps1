##Powershell Script to setup AD on Windows server 2012 or 2016
#this script will setup the base environment
# Import Variables
. ../Variables.ps1
##Setup AD
Install-WindowsFeature AD-Domain-Services -IncludeManagementTools
#This command has a section at the end to convert the password supplied to the right format
Install-ADDSForest -DomainName $mydomain -InstallDns -DatabasePath $mydbpath -SysvolPath $mysysvolpath -LogPath $mylogpath -SafeModeAdministratorPassword (ConvertTo-SecureString -String $myadminpw -AsPlainText -Force) -Force

#The Server will reboot here automatically, you can login to the domain using the Administrator account and the password set above.