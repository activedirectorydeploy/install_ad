##Powershell Script to setup AD on Windows server 2012 or 2016
#this script will setup the base environment
# Import Variables
. ../Variables.ps1

##Add Secure User, this step adds a secure user who is not administrator
#I have done this again to get Users and Computers Gui installed
Install-windowsfeature -name AD-Domain-Services –IncludeManagementTools

New-ADUser -Name $secuser -GivenName $secfirstname -Surname $secsurname -SamAccountName $secuser -UserPrincipalName $secuser@$mydomain

Set-ADAccountPassword ‘CN=$secuser,$ldapdc’ -Reset -NewPassword (ConvertTo-SecureString -AsPlainText $secpwd -Force)

Enable-ADAccount -Identity $secuser

Add-ADGroupMember ‘Domain Admins’ $secuser