Run after the AD setup these scripts provide user and group manegement in AD
We continue using the variables.ps1 file

step1_add_groups.ps1 / groups.csv

	Using the filename group.csv which needs to be in the same folder, or change the script to
	specify the location of the file


step2_bulkaddusers.ps1 / Userlist.csv

	Importing the users into the Users OU
	the username.csv i have supplied has the bare minimum to supply Windows to import a user 

	firstname	middlename	lastname	username	password	ou, 
 
	you can add extra columns if you also want to inclue the information below. If the script c	an't 
	find the information it just presents a null value.

step3_add_users_to_groups.ps1 / userstogroups.csv

	Probably the most confusing table of the three scripts, we can list grups in the 
	First column and the users we want in those security groups.
	
	the groups listed must have been created in the step1 script.


