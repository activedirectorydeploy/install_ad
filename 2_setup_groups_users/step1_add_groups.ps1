# Import Variables
. ../Variables.ps1
#I have add this import, if its already imported it will skip it
Import-Module ActiveDirectory
#Pulling the group names from the csv
$groups = Import-Csv groups.csv
foreach ($group in $groups) {
New-ADGroup -Name $group.name -Path $ldapdc -Description $bulkimport -GroupCategory Security -GroupScope Universal -Managedby $secuser
}