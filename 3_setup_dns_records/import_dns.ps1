# Import Variables
. ../Variables.ps1

Import-Csv .\arecords.csv | foreach{Add-DnsServerResourceRecordA -Name $_.name -ZoneName $mydomain -IPv4Address $_.ip -ComputerName $computername }
Import-Csv .\crecords.csv | foreach{Add-DnsServerResourceRecordCName -Name $_.name -ZoneName $mydomain -HostNameAlias $_.cname -ComputerName $computername}
