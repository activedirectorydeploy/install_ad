Add-WindowsFeature adcs-cert-authority
Install-AdcsCertificationAuthority -CAType EnterpriseRootCa -CryptoProviderName "ECDSA_P256#Microsoft Software Key Storage Provider" -KeyLength 256 -HashAlgorithmName SHA256 -Force
Add-WindowsFeature RSAT-ADCS,RSAT-ADCS-mgmt

