# Setup an AD server using powershell

## Scope
The scripts included in this repository are designed to install AD, Add users and Groups
Populate DNS and setup a CA from the Powershell command line in Windows Server.


## Details

The command lines were tested on Windows 2016 standard install, with Powershell open as
Administrator.

I have adapted the scripts to use the file Variables.ps1 which should be filled with the
site specific information relating to the install, this in turn makes the scripts able to 
work across multiple Domain installs as nothing is hard coded into the scripts.


## Scripts

I've broken the scripts into 4 sections, and each section is subsequently broken down
into smaller scripts for ease of troubleshooting. Also some of the scripts will require
Reboots.

Each Script Directory has a README.md explaining what im trying to do with the scripts in
that section.

